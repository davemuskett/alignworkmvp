#!/usr/bin/env bash

set -e -u -x

mv dependency-cache/node_modules alignworkmvp
cd alignworkmvp && npm run start:express
