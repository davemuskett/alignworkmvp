# Align.work MVP

Ensure you are running NodeJS v8.0.0 to ensure latest ES6 syntax can be used in the app (recommend to install NVM and then 'nvm install v8.0.0' and `nvm use v8.0.0`).

To start-up app:

- `npm run start:express`
- `npm run start:watchify`: starts a watch script that compiles and bundles client-side JS
- `npm run build:browserify`: starts a build script that compiles and bundles client-side JS
- `npm run test`: starts Jest test runner

CSS

Compiled by Express on-the-fly from `.scss` files in `public/stylesheets`

HTML

Compiled by Express from `.pug` files in `views/`
